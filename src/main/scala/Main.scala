import cats.Show.Shown.mat
import domain.User
import lib.dsl.ConfigBuilder
import lib.interpreters.ProgramCmd

case class Config(maybeKey: String = "", maybeNamespace: Option[String] = None, projects: List[String] = Nil)


object Main {
  def main(arguments: Array[String]): Unit = {
    val args = Array("--key=myKey", "--project=proj1", "--project=proj2")

    val config = ConfigBuilder(Config(), args)
      .param[String]("key")((x, c) => c.copy(maybeKey = x))
      .param[String]("namespace", default = Some("defaultNamespace"))((x, c) => c.copy(maybeNamespace = Some(x)))
      .multipleParams[String]("project")((xs, c) => c.copy(projects = c.projects ++ xs))
      .build

    println(config)
  }
}