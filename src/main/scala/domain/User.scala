package domain

case class User(
                 name: Option[String] = None,
                 surname: Option[String] = None,
                 age: Option[Int] = None
               )
