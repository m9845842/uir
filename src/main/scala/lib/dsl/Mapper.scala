package lib.dsl

trait Mapper[A] {
  def reads: String => A
}

object Mapper {
  def apply[A](implicit read: Mapper[A]): Mapper[A] = read

  implicit val stringRead: Mapper[String] = new Mapper[String] {
    def reads: String => String = identity
  }

  implicit val intRead: Mapper[Int] = new Mapper[Int] {
    def reads: String => Int = _.toInt
  }

  implicit val doubleRead: Mapper[Double] = new Mapper[Double] {
    def reads: String => Double = _.toDouble
  }
}
