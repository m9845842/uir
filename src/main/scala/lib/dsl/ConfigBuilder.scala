package lib.dsl

import lib.Transformer
import lib.interpreters.ProgramCmd

class ConfigBuilder[C, F](initialConfig: C, arguments: Array[String]) {
  private val argsList: Option[List[(String, String)]] = ProgramCmd.program(arguments)
  private var transformedTrait: Option[F] = None
  private var config: C = initialConfig
  private var multipleValues: Map[String, List[String]] = Map()

  def param[A: Mapper](name: String, default: Option[A] = None)(action: (A, C) => C): ConfigBuilder[C, F] = {
    val read = implicitly[Mapper[A]]
    val value = argsList.flatMap(_.find(_._1 == name).map(_._2)).map(read.reads).getOrElse(default.getOrElse(read.reads("")))
    config = action(value, config)
    this
  }

  def multipleParams[A: Mapper](name: String, default: List[A] = List.empty[A])(action: (List[A], C) => C): ConfigBuilder[C, F] = {
    val read = implicitly[Mapper[A]]
    val values = argsList.toList.flatten.filter(_._1 == name).map(_._2).map(read.reads)
    val finalValues = if (values.isEmpty) default else values
    multipleValues += (name -> finalValues.map(_.toString)) // Convert to string if needed
    config = action(finalValues, config)
    this
  }

  def cmd(name: String)(action: (C) => C): ConfigBuilder[C, F] = {
    if (arguments.contains(name)) {
      config = action(config)
    }
    this
  }

  def transform(f: List[String] => F) = {
    transformedTrait = Transformer.transform(argsList, f)
  }

  def getMultipleValues: Map[String, List[String]] = multipleValues

  def build: C = config
}

object ConfigBuilder {
  def apply[C, F](initialConfig: C, args: Array[String]): ConfigBuilder[C, F] = new ConfigBuilder[C, F](initialConfig, args)
}


