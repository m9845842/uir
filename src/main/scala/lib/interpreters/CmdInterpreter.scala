package lib.interpreters

import lib.{Program, ProgramAlgebra}

trait CmdInterpreter extends ProgramAlgebra {
  type TProgram = Array[String] => Option[List[(String, String)]]
  type TFunctionDecl = List[(String, String)] => Option[List[(String, String)]]
  type TFunctionImpl = List[(String, String)] => List[(String, String)]

  def emptyProgram(name: String): TProgram = _ => None

  def addFunction(
                   program: TProgram,
                   decl: TFunctionDecl,
                   impl: TFunctionImpl
                 ): TProgram =
    args => {
      val parsedArgs = parseArgs(args)
      program(args).orElse(decl(parsedArgs).map(_ => impl(parsedArgs)))
    }

  def functionDecl(name: String): TFunctionDecl =
    args => {
      if (args.map(_._1).forall(arg => args.exists(_._1 == arg))) Some(args)
      else None
    }

  def parseArgs(args: Array[String]): List[(String, String)] = {
    args.map { arg =>
      val parts = arg.split("=", 2)
      parts(0).stripPrefix("--") -> parts(1)
    }.toList
  }
}

object ProgramCmd extends Program with CmdInterpreter {
  def impl: List[(String, String)] => List[(String, String)] = identity
}