package lib.interpreters

import lib.{Program, ProgramAlgebra}

trait HelpInterpreter extends ProgramAlgebra {
  type TProgram = String
  type TFunctionDecl = String
  type TFunctionImpl = Unit

  def emptyProgram(name: String): String = s"\n\nProgram $name usage:"

  def addFunction(program: String, decl: String, impl: Unit): String =
    program + s"\n  <program> $decl"

  def functionDecl(name: String): TFunctionDecl =
    s"$name " + s"--$name <value>"
}

object ProgramHelp extends Program with HelpInterpreter {

  val impl: Unit = ()
}