package lib

trait Transformer {
  def transform[T](input: Option[List[(String, String)]], f: List[String] => T): Option[T]
}

class MyTransformer extends Transformer {
  override def transform[T](input: Option[List[(String, String)]], f: List[String] => T): Option[T] = {
    input.flatMap { list =>
      val argNames = list.map(_._1)
      if (argNames.forall(key => list.exists(_._1 == key))) {
        val values = argNames.flatMap(key => list.find(_._1 == key).map(_._2))
        if (values.length == argNames.length) Some(f(values)) else None
      } else {
        None
      }
    }
  }
}

object Transformer {
  implicit val optionTransformer: Transformer = new MyTransformer

  def transform[T](input: Option[List[(String, String)]], f: List[String] => T)(implicit transformer: Transformer): Option[T] = {
    transformer.transform(input, f)
  }
}