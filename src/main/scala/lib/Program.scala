package lib

trait Program extends ProgramAlgebra {
  def impl: TFunctionImpl

  def program: TProgram =
    addFunction(
      emptyProgram("program"),
      functionDecl("declaration"),
      impl
    )
}